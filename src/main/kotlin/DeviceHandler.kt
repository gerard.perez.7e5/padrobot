import java.io.BufferedInputStream

class DeviceHandler(private val controller: BufferedInputStream) {

    init {
        controller.readNBytes(184)
    }

    fun toJsEvent(): JsEvent {
        val input: List<Byte> = controller.readNBytes(8).asList()
        controller.readNBytes(8).asList()
        return JsEvent(
            value = input[4].toUShort(),
            type = input[6].toInt(),
            number = input[7].toUByte()
        )
    }

}