import org.apache.log4j.BasicConfigurator
import org.apache.xmlrpc.XmlRpcException
import org.apache.xmlrpc.server.PropertyHandlerMapping
import org.apache.xmlrpc.webserver.WebServer
import java.io.BufferedInputStream
import java.io.FileInputStream
import java.io.IOException

class XmlrpcServer {

    val device = startDevice()

    @Throws(XmlRpcException::class, IOException::class)
    fun start() {
        val sw = WebServer(8080)
        BasicConfigurator.configure()
        val mapping = PropertyHandlerMapping()
        mapping.addHandler("pad", XmlrpcServer::class.java)
        sw.xmlRpcServer.handlerMapping = mapping
        sw.start()
        println("Listening on port 8080")
    }

    fun startDevice(): DeviceHandler {
        val path = "/dev/input/js0"
        val devicecontoller = BufferedInputStream(FileInputStream(path))
        return DeviceHandler(devicecontoller)
    }

    fun getButton(): Int {
        var button  = -1
        while (true) {
            val button = device.toJsEvent().number.toInt()
            println(button)
            return button
        }
    }
}