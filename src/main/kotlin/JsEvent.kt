data class JsEvent(
//    val time: UByte,
    val value: UShort,
    val type: Int,
    val number: UByte
)